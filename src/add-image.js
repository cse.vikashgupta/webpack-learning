import image from './brown.jpg';
function addImage(){
    let img = document.createElement('img');
    img.width=300;
    img.alt="Brown Wooden";
    img.src=image;
    const body = document.querySelector('body');
    body.append(img);
}
export default addImage;