// import addImage from './add-image';
// import helloWorld from './hello-world';
// helloWorld();
// addImage();
import HelloWorldButton from "./components/HelloWorldButton/hello-world-button.js";
import Heading from "./components/Heading/Heading";
import ImageComponent from "./components/ImageComponent/image-component.js"
const hWButton = new HelloWorldButton();
const heading = new Heading();
const imgC = new ImageComponent();
hWButton.render();
heading.render();
imgC.render();