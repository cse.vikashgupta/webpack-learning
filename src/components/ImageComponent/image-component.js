import image from '../../brown.jpg';
import "./image-component.scss";
import _ from "lodash";
function addImage(){
    let img = document.createElement('img');
    img.classList.add('img');
    img.alt="Brown Wooden";
    img.src=image;
    const body = document.querySelector('body');
    body.append(img);
}
export default addImage;