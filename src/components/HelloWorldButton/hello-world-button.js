import './hello-world-button.scss';
class HelloWorldButton{
    buttonClassName = 'helloWorldButton';
    render(){
        const button = document.createElement('button');
        button.innerHTML='Hello World';
        button.classList.add(this.buttonClassName);
        const body = document.querySelector('body');
        body.append(button);
        button.onclick = function(){
            const p = document.createElement('p');
            p.innerHTML = 'Hello World';
            p.classList.add('helloWorldText');
            body.append(p);
        }
    }
}
export default HelloWorldButton;