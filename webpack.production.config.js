var path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports={
 entry:{
     'image-component':'./src/components/ImageComponent/image-component.js',
     'index':'./src/index.js'
 },
 output:{
     filename:'[name].[contenthash].js',
     path:path.resolve(__dirname,'./dist'),
     publicPath:''
 },
 optimization: {
     splitChunks: {
       chunks: 'all',
     },
 },
 mode:'production',
 module:{
     rules:[
         {
             test:/\.(png|jpg)$/,
             use:['file-loader']
         },
         {
             test:/\.css$/,
             use:[MiniCssExtractPlugin.loader,'css-loader']
         },
         {
             test:/\.scss$/,
             use:[
                 MiniCssExtractPlugin.loader,'css-loader','sass-loader'
             ]
         },
         {
             test:/\.js$/,
             exclude: '/node_modules/',
             use:{
                 loader:'babel-loader',
                 options:{
                     presets:['@babel/env'],
                     plugins:['transform-class-properties']
                 }
             }
         }
     ]
 },
 plugins:[
     new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
     }),
     new CleanWebpackPlugin({
         cleanOnceBeforeBuildPatterns:[
             '**/*'
         ]
     }),
     new HtmlWebpackPlugin({
        title: 'My App',
        filename: 'index.html'
      })
 ]   
}